import java.util.*;
import java.util.stream.IntStream;

public class Runner {

    private int tableHeight;
    private int tableWidth;
    private int[] shiftByKey;

    public Runner(String srcString, String keyWord){
        this.tableHeight = (int)Math.ceil((double)srcString.length()/(double)keyWord.length());
        this.tableWidth = keyWord.length();

        shiftByKey = new int[keyWord.length()];
        IntStream.range(0, keyWord.length()).forEach(i ->
                shiftByKey[i] = Character.getNumericValue(keyWord.toCharArray()[i]));
    }

    public static void main(String[] args) {

        String input = "top secret information - erase before read!";
        System.out.println("Input string : " + input);

        Runner runner = new Runner(input, "201");
        System.out.println("key : " + Arrays.toString(runner.shiftByKey));

        StringBuilder encryptResultString = new StringBuilder();

        IntStream.range(0, runner.tableHeight)
                .forEach(width -> IntStream.range(0, runner.tableWidth)
                .forEach(height -> {
                    if(width + runner.shiftByKey[height] * runner.tableHeight < input.length()) {
                        encryptResultString.append
                                (input.charAt(width + runner.shiftByKey[height] * runner.tableHeight));
                    }else {
                        encryptResultString.append('\0');
                    }
                }));

        System.out.println("----------------------------------------");

        System.out.println("Encryption result: " + encryptResultString);

        System.out.println("----------------------------------------");

        char[] decryptResult = new char[encryptResultString.length()];
        StringBuilder beatifulDecryptResult = new StringBuilder();

        IntStream.range(0, runner.tableWidth)
                .forEach(height -> IntStream.range(0, runner.tableHeight)
                        .forEach(width -> {
                            decryptResult[runner.shiftByKey[height] * runner.tableHeight + width] =
                                    encryptResultString.charAt(width * runner.tableWidth + height);
                        }));

        IntStream.range(0, decryptResult.length).forEach(i -> beatifulDecryptResult.append(decryptResult[i]));
        System.out.println("Decryption result: " + beatifulDecryptResult);
    }
}
